﻿using System;
using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
    public class ItemTests
    {

        [Fact]
        public void Equip_WeaponWithTooHighRequiredLevel_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior equipper = new Warrior();
            Weapon testAxe = new Weapon() { Name = "Common Axe", ItemSlot = Slot.Weapon, RequiredLevel = 2, WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 7 }, WeaponType = Weapon.Types.Axe };

            //Act
            InvalidWeaponException ex = Assert.Throws<InvalidWeaponException>(() => equipper.Equip(testAxe, equipper.AllowedWeaponTypes));

            //Assert
            Assert.Equal("You are not allowed to equip this weapon!", ex.Message);
        }

        [Fact]
        public void Equip_ArmorWithTooHighRequiredLevel_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior equipper = new Warrior();
            Armor testPlateBody = new Armor() { Name = "Common plate boody armor", RequiredLevel = 2, ArmorType = Armor.Types.Plate, Attributes = new PrimaryAttribute() { Strength = 1 }, ItemSlot = Slot.Body};
            
            //Act
            InvalidArmorException ex = Assert.Throws<InvalidArmorException>(() => equipper.Equip(testPlateBody, equipper.AllowedArmorTypes));

            //Assert
            Assert.Equal("You are not allowed to equip this armor!", ex.Message);
        }

        [Fact]
        public void Equip_WeaponOfWrongType_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior equipper = new Warrior();
            Weapon testBow = new Weapon() { Name = "Common Bow", RequiredLevel = 1, ItemSlot = Slot.Weapon, WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8}, WeaponType = Weapon.Types.Bow };
            //Act
            InvalidWeaponException ex = Assert.Throws<InvalidWeaponException>(() => equipper.Equip(testBow, equipper.AllowedWeaponTypes));

            //Assert
            Assert.Equal("You are not allowed to equip this weapon!", ex.Message);
        }

        [Fact]
        public void Equip_ArmorOfWrongType_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior equipper = new Warrior();
            Armor testClothHead = new Armor() { Name = "Common cloth head armor", RequiredLevel = 1, ItemSlot = Slot.Head, ArmorType = Armor.Types.Cloth, Attributes = new PrimaryAttribute() { Intelligence = 5 } };
            //Act
            InvalidArmorException ex = Assert.Throws<InvalidArmorException>(() => equipper.Equip(testClothHead, equipper.AllowedArmorTypes));

            //Assert
            Assert.Equal("You are not allowed to equip this armor!", ex.Message);
        }

        [Fact]
        public void Equip_AllowedWeaponType_ShouldReturnSuccessMessage()
        {
            //Arrange
            Warrior equipper = new Warrior();
            Weapon testAxe = new Weapon() { Name = "Common Axe", ItemSlot = Slot.Weapon, RequiredLevel = 1, WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 7 }, WeaponType = Weapon.Types.Axe };
            string expected = "New weapon equipped!";

            //Act
            string actual = equipper.Equip(testAxe, equipper.AllowedWeaponTypes);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_AllowedArmorType_ShouldReturnSuccessMessage()
        {
            //Arrange
            Warrior equipper = new Warrior();
            Armor testPlateBody = new Armor() { Name = "Common plate boody armor", RequiredLevel = 1, ArmorType = Armor.Types.Plate, Attributes = new PrimaryAttribute() { Strength = 1 }, ItemSlot = Slot.Body };
            string expected = "New armor equipped!";

            //Act
            string actual = equipper.Equip(testPlateBody, equipper.AllowedArmorTypes);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_WithNoWeaponEquipped_ShouldReturnOnePointZeroFive()
        {
            //Arrange
            Warrior noWeaponWarrior = new Warrior();
            double expected = 1.05;

            //Act
            double actual = noWeaponWarrior.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_WithValidWeapon_ShouldReturnEightPointZeroEightFive()
        {
            //Arrange
            Warrior equipper = new Warrior();
            Weapon testAxe = new Weapon() { Name = "Common Axe", ItemSlot = Slot.Weapon, RequiredLevel = 1, WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 7 }, WeaponType = Weapon.Types.Axe };
            equipper.Equip(testAxe, equipper.AllowedWeaponTypes);
            double expected = 8.085;

            //Act
            double actual = equipper.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_WithValidWeaponAndValidArmor_ShouldReturnEightPointOneSixTwo()
        {
            //Arrange
            Warrior equipper = new Warrior();
            Weapon testAxe = new Weapon() { Name = "Common Axe", ItemSlot = Slot.Weapon, RequiredLevel = 1, WeaponAttributes = new WeaponAttributes() { AttackSpeed = 1.1, Damage = 7 }, WeaponType = Weapon.Types.Axe };
            Armor testPlateBody = new Armor() { Name = "Common plate boody armor", RequiredLevel = 1, ArmorType = Armor.Types.Plate, Attributes = new PrimaryAttribute() { Strength = 1 }, ItemSlot = Slot.Body };
            equipper.Equip(testPlateBody, equipper.AllowedArmorTypes);
            equipper.Equip(testAxe, equipper.AllowedWeaponTypes);
            double expected = 8.162;

            //Act
            double actual = equipper.CalculateDamage();

            //Assert
            Assert.Equal(expected, actual);
        }


    }
}
