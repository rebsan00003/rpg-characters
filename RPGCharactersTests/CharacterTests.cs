﻿using RPGCharacters;
using System;
using Xunit;

namespace RPGCharactersTests
{
    public class CharacterTests
    {
        //Test if a character is level one when created
        [Fact]
        public void Warrior_CreatingAnInstanceOfACharacter_ShouldBeLevelOne()
        {
            //Arrange
            Warrior createdWarrior;
            int expected = 1;

            //Act
            createdWarrior = new Warrior();

            //Assert
            Assert.Equal(expected, createdWarrior.Level);
        }

        [Fact]
        public void Warrior_CreatingAnInstanceOfWarriorWithLevelOneStats_ShouldHaveStrengthFiveDexterityTwoIntelligenceOne()
        {
            //Arrange
            Warrior createdWarrior;
            int[] expected = new int[] { 5, 2, 1 };

            //Act
            createdWarrior = new Warrior();

            //Assert
            Assert.Equal(expected, new int[]{ createdWarrior.PrimaryAttribute.Strength, createdWarrior.PrimaryAttribute.Dexterity, createdWarrior.PrimaryAttribute.Intelligence});
        }

        [Fact]
        public void Mage_CreatingAnInstanceOfMageWithLevelOneStats_ShouldHaveStrengthOneDexterityOneIntelligenceEight()
        {
            //Arrange
            Mage createdMage;
            int[] expected = new int[] { 1, 1, 8 };

            //Act
            createdMage = new Mage();

            //Assert
            Assert.Equal(expected, new int[] { createdMage.PrimaryAttribute.Strength, createdMage.PrimaryAttribute.Dexterity, createdMage.PrimaryAttribute.Intelligence });
        }

        [Fact]
        public void Ranger_CreatingAnInstanceOfRangerWithLevelOneStats_ShouldHaveStrengthOneDexteritySevenIntelligenceOne()
        {
            //Arrange
            Ranger createdRanger;
            int[] expected = new int[] { 1, 7, 1 };

            //Act
            createdRanger = new Ranger();

            //Assert
            Assert.Equal(expected, new int[] { createdRanger.PrimaryAttribute.Strength, createdRanger.PrimaryAttribute.Dexterity, createdRanger.PrimaryAttribute.Intelligence });
        }

        [Fact]
        public void Rogue_CreatingAnInstanceOfRogueWithLevelOneStats_ShouldHaveStrengthTwoDexteritySixIntelligenceOne()
        {
            //Arrange
            Rogue createdRogue;
            int[] expected = new int[] { 2, 6, 1 };

            //Act
            createdRogue = new Rogue();

            //Assert
            Assert.Equal(expected, new int[] { createdRogue.PrimaryAttribute.Strength, createdRogue.PrimaryAttribute.Dexterity, createdRogue.PrimaryAttribute.Intelligence });
        }

        [Fact]
        public void LevelUp_IncreasingWarriorsLevelAndStats_ShouldIncreaseLevelToTwoAndShouldHaveStrengthEightDexterityFourIntelligenceTwo()
        {
            //Arrange
            Warrior levellingUp = new Warrior();
            int[] expected = new int[] { 2, 8, 4, 2 };

            //Act
            levellingUp.LevelUp();

            //Assert
            Assert.Equal(expected, new int[] { levellingUp.Level, levellingUp.PrimaryAttribute.Strength, levellingUp.PrimaryAttribute.Dexterity, levellingUp.PrimaryAttribute.Intelligence });
        }

        [Fact]
        public void LevelUp_IncreasingMageLevelAndStats_ShouldIncreaseLevelToTwoAndShouldHaveStrengthTwoDexterityTwoIntelligenceThirteen()
        {
            //Arrange
            Mage levellingUp = new Mage();
            int[] expected = new int[] { 2, 2, 2, 13 };

            //Act
            levellingUp.LevelUp();

            //Assert
            Assert.Equal(expected, new int[] { levellingUp.Level, levellingUp.PrimaryAttribute.Strength, levellingUp.PrimaryAttribute.Dexterity, levellingUp.PrimaryAttribute.Intelligence });
        }

        [Fact]
        public void LevelUp_IncreasingRangerLevelAndStats_ShouldIncreaseLevelToTwoAndShouldHaveStrengthTwoDexterityTwelveIntelligenceTwo()
        {
            //Arrange
            Ranger levellingUp = new Ranger();
            int[] expected = new int[] { 2, 2, 12, 2 };

            //Act
            levellingUp.LevelUp();

            //Assert
            Assert.Equal(expected, new int[] { levellingUp.Level, levellingUp.PrimaryAttribute.Strength, levellingUp.PrimaryAttribute.Dexterity, levellingUp.PrimaryAttribute.Intelligence });
        }

        [Fact]
        public void LevelUp_IncreasingRogueLevelAndStats_ShouldIncreaseLevelToTwoAndShouldHaveStrengthThreeDexterityTenIntelligenceTwo()
        {
            //Arrange
            Rogue levellingUp = new Rogue();
            int[] expected = new int[] { 2, 3, 10, 2 };

            //Act
            levellingUp.LevelUp();

            //Assert
            Assert.Equal(expected, new int[] { levellingUp.Level, levellingUp.PrimaryAttribute.Strength, levellingUp.PrimaryAttribute.Dexterity, levellingUp.PrimaryAttribute.Intelligence });
        }



    }
}
