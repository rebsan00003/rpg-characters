﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class PrimaryAttribute
    {
        private int strength;
        public int Strength
        {
            get { return strength; }
            set { strength = value; }
        }

        private int dexterity;
        public int Dexterity
        {
            get { return dexterity; }
            set { dexterity = value; }
        }

        private int intelligence;
        public int Intelligence
        {
            get { return intelligence; }
            set { intelligence = value; }
        }

    }
}
