﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Warrior : Character 
    {

        private Weapon.Types[] allowedWeaponTypes = { Weapon.Types.Axe, Weapon.Types.Hammer , Weapon.Types.Sword };
        private Armor.Types[] allowedArmorTypes = { Armor.Types.Mail, Armor.Types.Plate };

        public Weapon.Types[] AllowedWeaponTypes
        {
            get { return allowedWeaponTypes; }
        }

        public Armor.Types[] AllowedArmorTypes
        {
            get { return allowedArmorTypes; }
        }

        public Warrior()
        {
            PrimaryAttribute = new PrimaryAttribute()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1
            };
        }
        
        //Strength is Warriors Primary attribute
        public override double CalculateTotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor equippedArmor = (Armor)Equipment[item.Key];
                    TotalPrimaryAttributes += equippedArmor.Attributes.Strength;
                }
            }
            TotalPrimaryAttributes += PrimaryAttribute.Strength;
            return TotalPrimaryAttributes;
        }

        public override void LevelUp()
        {
            PrimaryAttribute.Strength += 3;
            PrimaryAttribute.Dexterity += 2;
            PrimaryAttribute.Intelligence += 1;
            Level++;
        }
    }
}
