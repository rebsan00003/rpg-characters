﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Rogue : Character 
    {

        private Weapon.Types[] allowedWeaponTypes = { Weapon.Types.Dagger, Weapon.Types.Sword };
        private Armor.Types[] allowedArmorTypes = { Armor.Types.Mail, Armor.Types.Leather };

        public Weapon.Types[] AloowedWeaponTypes
        {
            get { return allowedWeaponTypes; }
        }

        public Armor.Types[] AllowedArmorTypes
        {
            get { return allowedArmorTypes; }
        }

        public Rogue()
        {
            PrimaryAttribute = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1
            };
        }

        //Dexterity is Rogues primary attribute
        public override double CalculateTotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor equippedArmor = (Armor)Equipment[item.Key];
                    TotalPrimaryAttributes += equippedArmor.Attributes.Dexterity;
                }
            }
            TotalPrimaryAttributes += PrimaryAttribute.Dexterity;
            return TotalPrimaryAttributes;
        }

        public override void LevelUp()
        {
            PrimaryAttribute.Strength += 1;
            PrimaryAttribute.Dexterity += 4;
            PrimaryAttribute.Intelligence += 1;
            Level++;
        } 
    }
}
