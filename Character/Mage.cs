﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Mage : Character
    {

        private Weapon.Types[] allowedWeaponTypes = { Weapon.Types.Staff, Weapon.Types.Wand };
        private Armor.Types[] allowedArmorTypes = { Armor.Types.Cloth };
        
        public Weapon.Types[] AllowedWeaponTypes
        {
            get { return allowedWeaponTypes; }
        }

        public Armor.Types[] AllowedArmorTypes
        {
            get { return allowedArmorTypes; }
        }

        public Mage()
        {
            PrimaryAttribute = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8
            };
        }

        //Intelligence is Mages primary attribute
        public override double CalculateTotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor equippedArmor = (Armor)Equipment[item.Key];
                    TotalPrimaryAttributes += equippedArmor.Attributes.Intelligence;
                }
            }
            TotalPrimaryAttributes += PrimaryAttribute.Intelligence;
            return TotalPrimaryAttributes;
        }

        public override void LevelUp()
        {
            PrimaryAttribute.Strength += 1;
            PrimaryAttribute.Dexterity += 1;
            PrimaryAttribute.Intelligence += 5;
            Level++;
        }
    }
}
