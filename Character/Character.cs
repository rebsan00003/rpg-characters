﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Character
    {
        public Character()
        {
            Equipment = new Dictionary<Slot, Item>();
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private int level = 1;
        public int Level
        {
            get { return level; }
            set { level = value; }
        }

        private PrimaryAttribute primaryAttribute;
        public PrimaryAttribute PrimaryAttribute
        {
            get { return primaryAttribute; }
            set { primaryAttribute = value; }
        }

        private double totalPrimaryAttributes;
        public double TotalPrimaryAttributes 
        { 
            get { return totalPrimaryAttributes; }
            set { totalPrimaryAttributes = value; }
        }

        private Dictionary<Slot, Item> equipment;
        public Dictionary<Slot, Item> Equipment
        {
            get { return equipment; }
            set { equipment = value; }
        }

        /// <summary>
        /// Levels up the character with its own gain of each Primary attribute, also increases Level by one
        /// </summary>
        public abstract void LevelUp();

        /// <summary>
        /// Calculates the total attributes of the character, each type of character has own calculation -
        /// loops through the equipment and adds armors primary attributes to total + adds characters primary attribute to total
        /// </summary>
        /// <returns>Sum of total attributes</returns>
        public abstract double CalculateTotalAttributes();

        /// <summary>
        /// Calculates the total damage of the character, if weapon is equipped then add its DPS to the calculation-
        /// If no weapon is equipped then DPS is 1
        /// </summary>
        /// <returns>The calculated damage</returns>
        public double CalculateDamage()
        {
            double damage = 0;
            if (Equipment.ContainsKey(Slot.Weapon))
            {
                Weapon weapon = (Weapon)Equipment[Slot.Weapon];
                damage += weapon.CalculateDPS() * (1 + (CalculateTotalAttributes() / 100));
                return damage;
            }
            else
            {
                damage += 1 * (1 + (CalculateTotalAttributes() / 100));
                return damage;
            }
        }
        /// <summary>
        /// Method to Equip an item, takes in an item, and an array of unknown type (which will be the characters allowed weapon/armor types) -
        /// If the chosen item is of allowed type, characters level is equal/higher than item level and in correct slot then equip. -
        /// Otherwise throw custom exception
        /// </summary>
        /// <param name="item">The item the character tries to equip</param>
        /// <param name="types">Array of allowed types (either weapon types or armor types)</param>
        /// <returns>A success message that the item is equpped or throws exception</returns>
        public string Equip<T>(Item item, T[] types)
        {
            if (item is Weapon)
            {
                //Convert the item to Weapon to access the weapontype
                Weapon chosenWeapon = (Weapon)item;
                bool exists = Array.Exists(types, element => element.Equals(chosenWeapon.WeaponType));

                if (exists && Level >= item.RequiredLevel && item.ItemSlot == Slot.Weapon)
                {
                    Equipment[item.ItemSlot] = item;
                    return "New weapon equipped!";
                }
                else
                {
                    throw new InvalidWeaponException("You are not allowed to equip this weapon!");
                }
            }
            else if(item is Armor)
            {
                //Convert the item to Armor to access armortype
                Armor chosenArmor = (Armor)item;
                bool exists = Array.Exists(types, element => element.Equals(chosenArmor.ArmorType));

                if (exists && Level >= item.RequiredLevel && item.ItemSlot != Slot.Weapon)
                {
                    Equipment[item.ItemSlot] = item;
                    return "New armor equipped!";
                }
                else
                {
                    throw new InvalidArmorException("You are not allowed to equip this armor!");
                }
            }
            else
            {
                return "This item does not have a valid type.";
            }
        }

        /// <summary>
        /// Creates a string containing the characters stats for a character sheet
        /// </summary>
        /// <returns>A string of the characters properties</returns>
        public string DisplayStatistics()
        {
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor equippedArmor = (Armor)Equipment[item.Key];
                    PrimaryAttribute.Strength += equippedArmor.Attributes.Strength;
                    PrimaryAttribute.Intelligence += equippedArmor.Attributes.Intelligence;
                    PrimaryAttribute.Dexterity += equippedArmor.Attributes.Dexterity;
                }
            }
            StringBuilder characterStats = new StringBuilder("**********************************************\nCharacters stats:\n");
            characterStats.Append("Your characters name is " + Name +"\n");
            characterStats.Append("Your characters level is " + Level + "\n");
            characterStats.Append("Your characters strength is " + PrimaryAttribute.Strength + "\n");
            characterStats.Append("Your characters dexterity is " + PrimaryAttribute.Dexterity + "\n");
            characterStats.Append("Your characters intelligence is " + PrimaryAttribute.Intelligence + "\n");
            characterStats.Append("Your characters damage is " + CalculateDamage() + "\n");
            characterStats.Append("**********************************************");
            return characterStats.ToString();
        }
    }
}
