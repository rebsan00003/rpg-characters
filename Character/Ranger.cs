﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Ranger : Character
    {
        private Weapon.Types[] allowedWeaponTypes = { Weapon.Types.Bow };
        private Armor.Types[] allowedArmorTypes = { Armor.Types.Mail, Armor.Types.Leather};

        public Weapon.Types[] AloowedWeaponTypes
        {
            get { return allowedWeaponTypes; }
        }

        public Armor.Types[] AllowedArmorTypes
        {
            get { return allowedArmorTypes; }
        }

        public Ranger()
        {
            PrimaryAttribute = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1
            };
        }

        //Dexterity is Rangers primary attribute
        public override double CalculateTotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor equippedArmor = (Armor)Equipment[item.Key];
                    TotalPrimaryAttributes += equippedArmor.Attributes.Dexterity;
                }
            }
            TotalPrimaryAttributes += PrimaryAttribute.Dexterity;
            return TotalPrimaryAttributes;
        }
        
        public override void LevelUp()
        {
            PrimaryAttribute.Strength += 1;
            PrimaryAttribute.Dexterity += 5;
            PrimaryAttribute.Intelligence += 1;
            Level++;
        }
    }
}
