﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Item
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private Slot itemSlot;
        public Slot ItemSlot
        {
            get { return itemSlot; }
            set { itemSlot = value; }
        }

        private int requiredLevel;

        public int RequiredLevel
        {
            get { return requiredLevel; }
            set { requiredLevel = value; }
        }
    }
}
