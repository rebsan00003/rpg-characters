﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Weapon : Item
    {
        private Types weaponTypes;
        public Types WeaponType
        {
            get { return weaponTypes; }
            set { weaponTypes = value; }
        }

        private WeaponAttributes weaponAttributes;
        public WeaponAttributes WeaponAttributes
        {
            get { return weaponAttributes; }
            set { weaponAttributes = value; }
        }

        public enum Types
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }

        /// <summary>
        /// Calculates the weapons' damage per second
        /// </summary>
        /// <returns>Weapons DPS rounded to two decimal places</returns>
        public double CalculateDPS()
        {
            double totalDPS = WeaponAttributes.Damage * WeaponAttributes.AttackSpeed;
            return Math.Round(totalDPS, 2);
        }
  
    }
}
