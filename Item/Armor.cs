﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Armor : Item
    {
        private PrimaryAttribute attributes;
        public PrimaryAttribute Attributes
        {
            get { return attributes; }
            set { attributes = value;  }
        }

        private Types armorType;

        public Types ArmorType
        {
            get { return armorType; }
            set { armorType = value; }
        }

        public enum Types
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
    }
}
