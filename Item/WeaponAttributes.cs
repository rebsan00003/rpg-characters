﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class WeaponAttributes
    {
        private int damage;
        public int Damage
        {
            get { return damage; }
            set { damage = value; }
        }

        private double attackSpeed;
        public double AttackSpeed
        {
            get { return attackSpeed; }
            set { attackSpeed = value; }
        }
    }
}
