# RPG Characters

This is a Console Application containing game logic for a RPG Game. You can create characters of four different types: Mage, Ranger, Rogue and Warrior. These can equip items of either armor or weapon type. There is functionality for calculating damage, levelling up and displaying characters statistics. 

The solution also contains a project with tests for the item and character classes. 

## Install

Clone the repository to a local directory

```
git clone "https://gitlab.com/rebsan00003/rpg-characters.git"
cd rpg-characters
```
Open solution in Visual Studio.
Then run the application.

## Prerequisites

.NET Framework

Visual Studio 2017/19 OR Visual Studio Code


## Tests

To run the unit tests right click the RPGCharactersTests project and click "Run tests"

## Contributors

Rebecka Ocampo Sandgren @rebsan00003

## Contributing

No contributions allowed.
